import { Component } from "@angular/core";

/** This is only used in phone layouts
 * It doesn't follow the general smart/dumb comoponent split because
 * there is no web version of this - so we just combine it.
 */
@Component({
  selector: "secret-new-component",
  moduleId: module.id,
  template: `
    <ns-action-bar-container
      title="Add New Password"
    ></ns-action-bar-container>
    <StackLayout>
        <secret-form-container isNew="true"></secret-form-container>
    </StackLayout>
  `,
})
export class SecretNewComponent {
  constructor(
  ) {}
}

