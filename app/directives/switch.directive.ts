import { Directive, Input, forwardRef, HostListener, Renderer2, ElementRef } from '@angular/core';

import { FormViewAdapter, NGRX_FORM_VIEW_ADAPTER, FormControlState } from 'ngrx-forms';

@Directive({
  selector: 'Switch[ngrxFormControlState]',
  providers: [{
    provide: NGRX_FORM_VIEW_ADAPTER,
    useExisting: forwardRef(() => NgrxSwitchViewAdapter),
    multi: true,
  }],
})
export class NgrxSwitchViewAdapter implements FormViewAdapter {
  onChange: (value: any) => void = () => void 0;

  @HostListener('blur')
  onTouched: () => void = () => void 0

  constructor(private renderer: Renderer2, private elementRef: ElementRef) { }

  @Input() set ngrxFormControlState(value: FormControlState<any>) {
    if (!value) {
      throw new Error('The control state must not be undefined!');
    }
  }

  setViewValue(value: any): void {
    this.renderer.setProperty(this.elementRef.nativeElement, 'checked', value);
  }

  setOnChangeCallback(fn: (value: any) => void): void {
    this.onChange = fn;
  }

  setOnTouchedCallback(fn: () => void): void {
    this.onTouched = fn;
  }

  setIsDisabled(isDisabled: boolean): void {
    this.renderer.setProperty(this.elementRef.nativeElement, 'isEnabled', !isDisabled);
  }

  @HostListener('checkedChange', ['$event'])
  handleInput(event: any): void {
    this.onChange(event.object.checked);
  }
}
