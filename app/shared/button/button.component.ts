import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "app-button",
  template: `<Button
               class="button"
               [class.button-default]="!type || type === 'default'"
               [class.button-border]="type === 'border'"
               [class.button-text]="type === 'text'"
               [attr.width]="width ? width : null"
               [attr.height]="height ? height : null"
               [text]="text"
               [isEnabled]="isEnabled"
               (tap)="tap.emit()"
             ></Button>`,
  styles: [`.button {
              font-size: 14;
              letter-spacing: 0.06;
            }
            .button-default {
              background-color: #6CC780;
              color: white;
            }
            .button-border {
              background-color: #FFFFFF;
              border-width: 1;
              border-color: #0092A8;
              border-radius: 3;
              color: #0092A8;
              padding: 0 10;
            }
            .button-text {
              background-color: #FFFFFF;
              border-width: 1;
              border-color: transparent;
              color: #0092A8;
              padding: 0;
            }`],
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
  @Input() text: string;
  @Input() type: string;
  @Input() width: string;
  @Input() height: string;
  @Input() isEnabled: boolean = true;
  @Output() tap = new EventEmitter();

  constructor() { }
}
