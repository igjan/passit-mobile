import { Component, ChangeDetectionStrategy, Input } from "@angular/core";
import { AbstractControlState } from "ngrx-forms";

@Component({
  selector: "app-checkbox",
  templateUrl: "./checkbox.component.html",
  styles: [`
    .app-switch {
      width: 100;
      background-color: #6F989E;
      color: #6F989E;
    }
    .app-switch[checked=true] {
      width: 100;
      background-color: #0092A8;
      color: #0092A8;
    }
  `],
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxComponent {
  @Input() title: string;
  @Input() subtext: string;
  @Input() inline = false;
  @Input() control: AbstractControlState<boolean>;
}
