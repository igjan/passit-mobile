import { DeviceType } from "ui/enums";
import { device } from "platform";

export const IS_TABLET = device.deviceType === DeviceType.Tablet;
