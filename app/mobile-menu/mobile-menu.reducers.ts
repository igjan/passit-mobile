import { createFeatureSelector, createSelector } from "@ngrx/store";

import { MobileMenuActions } from "./mobile-menu.actions";

export interface IMobileMenuState {
  isEnabled: boolean;
}

export const initialState: IMobileMenuState = {
  isEnabled: false,
}

export function mobileMenuReducer(state=initialState, action: MobileMenuActions): IMobileMenuState {

  switch (action.type) {
    default:
      return state;
  }
}

export const getMobileMenuState = createFeatureSelector<IMobileMenuState>("mobile-menu");

const getShowMenu = (state: IMobileMenuState) => state.isEnabled;
export const getMobileMenuShowMenu = createSelector(getMobileMenuState, getShowMenu);
