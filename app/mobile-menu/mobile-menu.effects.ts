import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";

import { ToggleMenu, MobileMenuActionTypes } from "./mobile-menu.actions";
import { SideDrawer } from "./sidedrawer";
import { tap } from "rxjs/operators";

@Injectable()
export class MobileMenuEffects {

  /**
   * :( We can't control the side menu declaratively.
   * We can't determine the current state it's in (open or closed)
   * All we can do is toggle it - which makes it a side effect.
   */
  @Effect({ dispatch: false })
  toggleMenu$ = this.actions$.pipe(
    ofType<ToggleMenu>(MobileMenuActionTypes.TOGGLE_MENU),
    tap(() => {
      this.sidedrawer.toggle();
    })
  );

  constructor(
    private actions$: Actions,
    private sidedrawer: SideDrawer, 
  ) {}
}
