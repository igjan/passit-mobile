import { IAccountState } from "./passit-frontend/account/account.reducer";
import PassitSDK from "passit-sdk-js";
import { ISecretState } from "./passit-frontend/secrets/secrets.reducer";
import { device } from "tns-core-modules/platform/platform";

require( "nativescript-localstorage" );
var app = require('application');

// Java imports
const FillResponse = android.service.autofill.FillResponse;
const Dataset = android.service.autofill.Dataset;
const AutofillValue = android.view.autofill.AutofillValue;
const RemoteViews = android.widget.RemoteViews;
const View = android.view.View;


/**
 * This class helps read in Passit's state and matches android app names
 * to potential matches in Passit. It doesn't depend on Angular because we
 * cannot reliably call Angular code from a Android service process.
 */
class AutofillPassitHelper {
  account: IAccountState;
  secrets: ISecretState;
  sdk = new PassitSDK();

  /** 
   * Run this before using the helper - it loads data from localstorage and preps the SDK
   * The data loading step is a bit too simple - it just reads the state from the apps
   * reserved filesystem (using a localstorage polyfill to be more js like)
   * 
   * This means it may not always be the most up to date.
   * It also depends on the private key being written to disk. This would have to change
   * if we wanted to force the key to never be written unencrypted to disk (for a unlock your
   * vault type of scenario)
   */
  async setUp() {
    this.account = JSON.parse(localStorage.getItem('account'));
    this.secrets = JSON.parse(localStorage.getItem('secrets'));
    this.sdk.set_url(this.account.auth.url);
    await this.sdk.set_up(
      this.account.auth.publicKey,
      this.account.auth.privateKey,
      this.account.auth.userId,
      this.account.auth.userToken
    );
    return;
  }

  /**
   * This matches the android app to some Passit secrets.
   * For any matched results - we have to decrypt here because the AutofillValue expects text, not a function
   * @param idPackage Android Package ID
   */
  async getUsernamePasswords(idPackage: string): Promise<UserData[]> {
    const secrets = Object.values(this.secrets.entities);
    const matchedSecrets = secrets.filter((secret) => {
      // Check if package name looks like the secret's name
      let passitName = secret.name.toLowerCase();
      let passitUrl = secret.data.url ? secret.data.url.toLowerCase() : null;
      for (let part of idPackage.toLowerCase().split('.')) {
        if (part !== "com" && part.length > 2) {
          if (passitName.includes(part)) {
            // console.log('match name', part, 'and', passitName)
            return true;
          } else if (passitUrl && passitUrl.includes(part)) {
            // console.log('match url', part, 'and', passitUrl)
            return true;
          }
        }
      };
    });

    let results = await Promise.all(matchedSecrets.map(async (secret): Promise<UserData> => {
      let decrypted = await this.sdk.decrypt_secret(secret);
      return {
        username: secret.data.username,
        password: decrypted.password,
      };
    }));
    return results;
  }
}

/** Expected structure for an autofill Login Form */
interface ParsedStructure {
  usernameId: android.view.autofill.AutofillId;
  passwordId: android.view.autofill.AutofillId;
}

/** Text values for a login form */
interface UserData {
  username: string;
  password: string;
}

/** 
 * Parser for a Android AssistStructure. Traverses nodes looking for matching forms that might be autofillable
 */
class AutofillParser {
  /** Android package name, like com.nativescript */
  idPackage: string;
  /** Fields that might be autofillable */
  private fieldCollection: android.app.assist.AssistStructure.ViewNode[] = [];

  constructor(private structure: android.app.assist.AssistStructure) {}

  parse() {
    for (let i = 0; i < this.structure.getWindowNodeCount(); i++) {
        let node = this.structure.getWindowNodeAt(i);
        this.parseNode(node.getRootViewNode());
    }
  }

  /** Recursive function to parse all nodes while looking for relevant (autofillable) nodes */
  private parseNode(node: android.app.assist.AssistStructure.ViewNode) {
    // Is the node a native or web text input?
    let hints = node.getAutofillHints();
    let isEditText = node.getClassName() === "android.widget.EditText" || (node.getHtmlInfo() && node.getHtmlInfo().getTag() === "input");
    if (isEditText || (hints && hints.length > 0)) {
      this.fieldCollection.push(node);
    }
    for(let i = 0; i < node.getChildCount(); i++) {
      this.parseNode(node.getChildAt(i));
    }
  }

  private checkForUsernameField(field: android.app.assist.AssistStructure.ViewNode) {
    let hints: string[] = field.getAutofillHints();
    let fieldName = field.getIdEntry();
    if (hints) {
      for(let i = 0; i < hints.length; i++) {
        const hint = hints[i];
        // Check for canonical Android autofill hints
        if (hint === View.AUTOFILL_HINT_USERNAME) {
          return field;
        }
      }
    }
    if (fieldName && fieldName.includes('identifier')) {
      return field;
    }
  }

  private checkForPasswordField(field: android.app.assist.AssistStructure.ViewNode) {
    let hints: string[] = field.getAutofillHints();
    let fieldName = field.getIdEntry();
    if (hints) {
      for(let i = 0; i < hints.length; i++) {
        const hint = hints[i];
        // Check for canonical Android autofill hints
        if ([View.AUTOFILL_HINT_PASSWORD, 'current-password'].includes(hint)) {
          return field;
        }
      }
    }
    if (fieldName && fieldName.includes('password')) {
      return field;
    }
  }

  /**
   * Check if the structure contains any login forms and return the form is so. Otherwise return void.
   * This function also sets teh idPackage.
   */
  checkForLogin(): ParsedStructure | void {
    let usernameNode: android.app.assist.AssistStructure.ViewNode;
    let passwordNode: android.app.assist.AssistStructure.ViewNode;
    for (let field of this.fieldCollection) {
      if (usernameNode === undefined) {
        usernameNode = this.checkForUsernameField(field);
      }
      if (passwordNode === undefined) {
        passwordNode = this.checkForPasswordField(field);
      }
    };
    // Only return the form if both are found. Multi step forms are not supported at this time.
    if (usernameNode && passwordNode) {
      let parsedStructure = {
        usernameId: usernameNode.getAutofillId(),
        passwordId: passwordNode.getAutofillId(),
      };
      // Note the package id of the app the form is found on.
      this.idPackage = usernameNode.getIdPackage();
      // usernameNode.getWebDomain();
      return parsedStructure;
    }
  }
}

// If this code runs at all in android < 8 (sdk 26) it crashes!
if (parseInt(device.sdkVersion) >= 26) {
/**
 * This is the NativeScript way to register a Android service.
 * This service is where everything happens - everything else is just supporting this.
 * We need to define what happens on a fill request and on save request.
 * This class has to support being closed and reopened at any time. It can't make imports from the rest of the Angular app.
 */
(android.service.autofill.AutofillService as any).extend("com.tns.passit.AutofillService", {
  // https://developer.android.com/reference/android/service/autofill/AutofillService.html#onFillRequest(android.service.autofill.FillRequest,%20android.os.CancellationSignal,%20android.service.autofill.FillCallback)
  onFillRequest: function(
    request: android.service.autofill.FillRequest,
    CancellationSignal: android.os.CancellationSignal,
    callback: android.service.autofill.FillCallback
  ) {
    // First get and parse the form's app structure.
    const context = request.getFillContexts();
    const structure: android.app.assist.AssistStructure = (context.get(context.size() - 1) as any).getStructure();
    const parser = new AutofillParser(structure);
    parser.parse();
    const parsedStructure = parser.checkForLogin();

    // No form to fill, return
    if (!parsedStructure) {
      return;
    }

    // Since a form was found, bootstrap passit and look for potential matches
    const packageName = app.android.context.getPackageName();
    const helper = new AutofillPassitHelper();
    helper.setUp().then(() => {
      // Find matches
      helper.getUsernamePasswords(parser.idPackage).then((userDatas) => {

        // If passit has 0 suggestions, just return
        if (userDatas.length === 0) return;

        // Build the options for autofill to present to the user
        const fillResponse = new FillResponse.Builder()
        userDatas.forEach((userData) => {
          const usernamePresentation = new RemoteViews(packageName, android.R.layout.simple_list_item_1);
          usernamePresentation.setTextViewText(android.R.id.text1, userData.username);
          const passwordPresentation = new RemoteViews(packageName, android.R.layout.simple_list_item_1);
          passwordPresentation.setTextViewText(android.R.id.text1, "Password for " + userData.username);
          fillResponse.addDataset(new Dataset.Builder()
            .setValue(parsedStructure.usernameId, AutofillValue.forText(userData.username), usernamePresentation)
            .setValue(parsedStructure.passwordId, AutofillValue.forText(userData.password), passwordPresentation)
            .build())
        });
        callback.onSuccess(fillResponse.build());
      });
    });
  },

  // https://developer.android.com/reference/android/service/autofill/AutofillService.html#onSaveRequest(android.service.autofill.SaveRequest,%20android.service.autofill.SaveCallback)
  onSaveRequest: function(SaveRequest: android.service.autofill.SaveRequest, SaveCallback: android.service.autofill.SaveCallback) {
    console.log('on save request not implemented');
  }
});
}